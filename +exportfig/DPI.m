function ret = DPI( dpi_ );
    persistent p;
    if( nargin );
        if( isempty(dpi_) );
            p = [];
        elseif( ~ischar(p) && ~isstring(p) );
            p = ['-r' num2str(dpi_)];
        else;
            assert( strcmp( p(1:2), '-r' ) );
            p = char(p); end;
    else;
        if( isempty(p) );
            ret = '-r600'; 
        else;
            ret = p; end; end; %resolution
