function LABEL( X, Y, Z, C );
if( nargin==0 );
    xlabel( '' );
    ylabel( '' );
    return; end;
if( nargin>=1 );
    xlabel(X,'Interpreter','Latex'); end;
if( nargin>=2 );
    ylabel(Y,'Interpreter','Latex'); end;
if( nargin>=3 );
    zlabel(Z,'Interpreter','Latex'); end;
if( nargin>=4 );
    cb = findall(gcf,'type','ColorBar');;
    if( isempty(cb) );
        cb = colorbar; end;    
    lab = ylabel( cb, C ); 
    lab.Interpreter = 'latex'; end;
    
