function f = FIG( num );
    warning('off','MATLAB:MKDIR:DirectoryExists');
    warning('off','MATLAB:LargeImage');

    set( groot, 'defaultAxesTickLabelInterpreter','latex' );
    set( groot, 'defaulttextinterpreter','latex' );
    set( groot, 'defaultLegendInterpreter','latex' );
    if( nargin>=1 );
        f = figure( num );
        f.Units = 'Centimeters';
        f.Position = [0 0 25 25];
    else;
        f = figure( 'Units','Centimeters', 'Position',[0 0 25 25] ); end;
        