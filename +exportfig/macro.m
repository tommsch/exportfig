import exportfig.*

%% save image - macros
clc;
warning('off','MATLAB:MKDIR:DirectoryExists');
warning('off','MATLAB:LargeImage');

set( groot, 'defaultAxesTickLabelInterpreter', 'latex' );  
set( groot, 'defaulttextinterpreter', 'latex' );
set( groot, 'defaultLegendInterpreter', 'latex' );

FIGURE = @() figure('Units','Centimeters','Position',[0 0 25 25]);
TITLE = @(T) title(T,'Interpreter','latex');
AXISE = @(A) eval('axis(''equal''); axis(A)');
AXIS = @(A) eval('axis(A)');
LABEL = @(X,Y) eval(['xlabel(X,''Interpreter'',''Latex'');'...
'ylabel(Y,''Interpreter'',''Latex'');']);
LABEL3 = @(X,Y,Z) eval(['xlabel(X,''Interpreter'',''Latex'');'...
'ylabel(Y,''Interpreter'',''Latex'');'...
'zlabel(Z,''Interpreter'',''Latex'');']);
TICKS = @(X,Y) eval('xticks(X); yticks(Y);');
TICKS3 = @(X,Y,Z) eval('xticks(X); yticks(Y); zticks(Z);');
TICKLABELS = @(X,Y) eval('xticklabels(X); yticklabels(Y);');
SIZE = @(X,Y) set(gca,'Units','Centimeters','Position',[3 3 3*X 3*Y]);
SIZE1 = @() set(gca,'Units','Centimeters','Position',[3 3 15 1]);
TEXT = @(X,Y,T) text(X, Y, T,'Interpreter','Latex','Fontsize',FONTSIZE);
POST = @() eval(['set(findall(gcf,''-property'',''FontSize''),''FontSize'',' num2str(FONTSIZE) ' );'...
'set(gca,''TickLabelInterpreter'', ''Latex'');'...
'set(gcf, ''Color'', ''w'');'...
'set(gca,''XMinorTick'',''off'',''YMinorTick'',''off'');'...
'box on']);    
SAVE = @(FOLDER,NAME,EXT) eval(['mkdir(fullfile('''',''' BASE ''', FOLDER));'...
'export_fig(fullfile('''', ''' BASE ''',FOLDER,NAME), ''' DPI ''', ''' AA ''', ''' EXT ''', ''-dCompatibilityLevel=1.4'');'...
'fprintf(''%s/%s finished\n=============================================\n=============================================\n'',FOLDER,NAME);']);
% %%%%%%%%%%%%%%%%%%%%%%%%