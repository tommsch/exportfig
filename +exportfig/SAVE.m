function SAVE( FOLDER, NAME, EXT_ );
    import exportfig.*
    POST()
    if( nargin== 2 );
        EXT_ = EXT; end;
    if( ~isequal(EXT_(1),'-') );
        EXT_ = ['-' EXT_]; end;
    BASE = evalin( 'caller', 'pwd;' );
    mkdir( fullfile( BASE , FOLDER ) );
    export_fig( fullfile(BASE,FOLDER,NAME), DPI, AA, EXT_, '-dCompatibilityLevel=1.4' );
    fprintf( '%s/%s finished\n=============================================\n=============================================\n', FOLDER, NAME );
    