function AXIS( A, C );
    ax = axis();
    if( numel(A)>=2 && isnan(A(1)) && isnan(A(2)) );
        A(1:2) = ax(1:2); end;
    if( numel(A)>=4 && isnan(A(3)) && isnan(A(4)) || numel(A)<=2 );
        A(3:4) = ax(3:4); end;
    if( numel(A)>=6 && isnan(A(6)) && isnan(A(6)) || numel(A)<=4 && numel(ax)>=6 );
        A(5:6) = ax(5:6); end;
    axis( A );
    if( nargin==1 && numel(A)>=8 );
        caxis( A(7:8) );
    elseif( nargin>=2 );
        caxis( C ); end;
    