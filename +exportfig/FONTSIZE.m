function ret = FONTSIZE( sze );

	if( nargin==0 || isempty(sze) );
	    sze = defaultsze; end;
    ret = sze;  % font size
    
end
    
function ret = defaultsze;
    ret = 8;
end