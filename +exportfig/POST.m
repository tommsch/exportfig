function POST;
    import exportfig.*
    set( findall(gcf,'-property','FontSize'), 'FontSize', FONTSIZE  );
    set( gca, 'TickLabelInterpreter', 'Latex' );
    set( gcf, 'Color','w' );
    set( gca, 'XMinorTick','off', 'YMinorTick','off' );
    box on
